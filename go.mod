module rabbitmq

go 1.16

require (
	github.com/rabbitmq/amqp091-go v1.3.4
	github.com/streadway/amqp v1.0.0
)
