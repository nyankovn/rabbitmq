package main

import (
	"log"
	"os"
	"rabbitmq/connection"
	"rabbitmq/util"

	"strings"

	"github.com/streadway/amqp"
)

func main() {
	ch, err := connection.EstablishConn()
	if err != nil {
		util.FailOnError(err, err.Error())
	}
	defer ch.Close()

	q, err := util.DeclareQueue(ch)

	util.FailOnError(err, "Failed to declare a queue")

	body := bodyFrom(os.Args)
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(body),
		})
	util.FailOnError(err, "Failed to declare a queue")
	log.Printf(" [x] Sent %s", body)
}

func bodyFrom(args []string) string {
	var s string
	if (len(args) < 2) || os.Args[1] == "" {
		s = "hello"
	} else {
		s = strings.Join(args[1:], " ")
	}
	return s
}
