package connection

import (
	amqp "github.com/streadway/amqp"
)

func EstablishConn() (channel *amqp.Channel, err error) {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		return nil, err
	}

	ch, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	return ch, nil
}
